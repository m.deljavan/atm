export const getFromLocaleStorage = (key) => {
    return JSON.parse(localStorage.getItem(key))
}

export const setToLocalStorage = (key, data) => {
    localStorage.setItem(key, JSON.stringify(data))
}