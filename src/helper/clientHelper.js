export const atm = (amount, notes) => {
    const totalMoney = amount
    notes.sort(function(a, b) {
        return b.note - a.note;
    });

    /** calculate count of notes **/
    const counts = [];
    notes.forEach(({ note, count }) => {
        let remain = Math.floor(amount / note);

        remain = Math.min(remain, +count);
        counts.push(remain);

        amount -= remain * note;
    });

    /** generate object **/
    const output = {};

    notes.forEach(({ id }, index) => {
        output[id] = counts[index];
    });

    return output;
};

export const validationTotal = (amount, notes) => {
    let totalMoney = 0;
    // get maximun count of notes from local storage
    notes.forEach(({ note, count }) => {
        totalMoney += note * count;
    });

    const result = {
        valid: amount <= totalMoney,
        totalMoney
    };
    return result;
};

export const validationPayNotes = (payData, amount, notes) => {
    do {
        let sum = 0;

        Object.entries(payData).forEach(([id, count]) => {
            const { note } = notes.find(note => note.id === id);
            sum += note * count;
        });

        if (sum === amount) {
            return {
                valid: true,
                payInfo: payData,
                amount
            };
        }

        if (notes.length === 0) {
            return { valid: false };
        }

        notes.shift();

        payData = atm(amount, notes);
    } while (true);
};
