import React, { Component } from "react";
import Employee from "../Customer/Employee";
import { ConfigProvider, Layout, Icon, Menu } from "antd";
import faIR from "antd/es/locale/fa_IR";
import Client from "../Client/Client";
const { Header, Footer, Content } = Layout;
class App extends Component {
    state = {
        clientPage: true,
        employeePage: false
    };
    onToggleEmployeeHandler = () => {
        this.setState({
            employeePage: true,
            clientPage: false
        });
    };

    onToggleClientHandler = () => {
        this.setState({
            employeePage: false,
            clientPage: true
        });
    };


    render() {
        return (
            <ConfigProvider locale={faIR}>
                <Layout className={"d-flex flex-column h-min-vh"}>
                    <Header className={"flex-grow-0"}>
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={["2"]}
                            style={{ lineHeight: "64px" }}
                        >
                            <Menu.Item onClick={this.onToggleEmployeeHandler} key="1">بخش کارمندان</Menu.Item>
                            <Menu.Item onClick={this.onToggleClientHandler} key="2">بخش مشتریان</Menu.Item>
                        </Menu>
                    </Header>
                    <Content className={"flex-grow-1"}>
                        {this.state.employeePage && <Employee />}
                        {this.state.clientPage && <Client />}
                    </Content>
                    <Footer className={"flex-grow-0"}></Footer>
                </Layout>
            </ConfigProvider>
        );
    }
}

export default App;
