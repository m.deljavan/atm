import React, {Component} from 'react';
import { Alert } from 'antd';

class Timer extends Component {
  state = {
    secs: this.props.duration || 20
  }
  tick() {
    this.setState(state => {
      return {
        secs: state.secs - 1
      }
    })
  }
  componentDidMount() {
    this.interval = setInterval(() => {
      this.tick()
        if(this.state.secs === 0) {
          clearInterval(this.interval);
          this.props.timeoutHandler()
        }
    }, 1000)
  }
  componentWillUnmount() {
    clearInterval(this.interval)
  }
  render() {
    const min = ('0' + Math.floor(this.state.secs / 60) % 60).slice(-2);
    const sec = ('0' + this.state.secs%60).slice(-2); 
    return (
      <Alert style={{textAlign: 'center', direction: "ltr"}} type={sec > 15 ? "success" : 'error'} message={<div>
        <span>{min}</span> : <span>{sec}</span>
      </div>}/>
    )
  }
}

export default Timer;