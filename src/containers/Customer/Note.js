import React, { Component } from "react";
import { Icon, Alert, List, Avatar } from "antd";

class Note extends Component {
  render() {
    const {
      onUndoTrashesNotesHandler,
      onShowModalHandler,
      onChangeNoteActivity,
      onChangeNoteCount,
      onClickDeleteNoteHandler
    } = this.props;
    const notTrashNotesActions = note => [
      <Icon type="delete" key="delete" title="حذف" onClick={() => onClickDeleteNoteHandler(note)} />,
      <Icon type="edit" key="edit" title="ویرایش" onClick={() => onShowModalHandler(note, "edit")} />,
      <Icon
        type={note.active ? "minus-circle" : "check-circle"}
        onClick={() => onChangeNoteActivity(note)}
        title={note.active ? "غیرفعال کردن" : "فعال کردن"}
      />
    ];
    const deletedNotesActions = note => [
      <Icon type={"undo"} onClick={() => onUndoTrashesNotesHandler(note)} title={"بازیابی"} />
    ];
    return (
      <>
        <List
          itemLayout="horizontal"
          dataSource={this.props.notes}
          renderItem={note => (
            <List.Item actions={this.props.trashed ? deletedNotesActions(note) : notTrashNotesActions(note)}>
              <List.Item.Meta
                style={{ alignItems: "center" }}
                avatar={
                  <Avatar
                    shape={"square"}
                    style={{
                      marginLeft: 14,
                      width: "150px",
                      height: "60px"
                    }}
                    src={note.imgUrl}
                  />
                }
                title={`${note.note}تومانی`}
              ></List.Item.Meta>
              <Alert
                style={{ flexGrow: 1, width: "40%", margin: "0 50px" }}
                type={note.count > 15 ? "success" : "error"}
                message={
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    تعداد اسکناس موجود در دستگاه:
                    <strong style={{ margin: "0 5px" }}>{note.count}</strong>
                  </div>
                }
              ></Alert>
            </List.Item>
          )}
        />
      </>
    );
  }
}

export default Note;
