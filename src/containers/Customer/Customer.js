import { Modal } from "antd";
import React, { Component } from "react";
import uuid from "uuid";
import Notes from "./Notes";
import NoteForm from "./NoteForm";
import { setToLocalStorage, getFromLocaleStorage } from "../../helper/localeStorage";

class Customer extends Component {
  state = {
    notes: [],
    payHistory: [],
    modal: {
      show: false,
      note: {}
    },
    formData: {}
  };

  /** change note activity */
  onChangeNoteActivity = ({ id }) => {
    const updatedNotes = this.state.notes.map(noteDetail => {
      const { active } = noteDetail;

      if (noteDetail.id === id) {
        return {
          ...noteDetail,
          active: !active
        };
      }

      return {
        ...noteDetail
      };
    });

    this.setState({ notes: updatedNotes });
    this.saveOnLocaleStorage(updatedNotes);
  };

  /** get input change count of note */
  onChangeNoteCount = e => {
    const updatedNotes = this.state.notes.map(noteDetail => {
      const { note } = noteDetail;
      const { value, name } = e.target;

      if (note === name) {
        return {
          ...noteDetail,
          changedCount: value
        };
      }

      return {
        ...noteDetail
      };
    });

    this.setState({
      notes: updatedNotes
    });
    this.saveOnLocaleStorage(updatedNotes);
  };

  /** handler for editing a note */
  onShowModalHandler = (note, state) => {
    this.setState({
      modal: {
        show: true,
        state,
        note
      }
    });
  };

  onSubmitFormNoteHandler = () => {
    const updatedNotes = this.state.notes.map(noteDetail => {
      const { id } = noteDetail;
      if (id === this.state.modal.note.id) {
        return {
          ...noteDetail,
          ...this.state.formData,
          imgUrl: this.state.formData.imgUrl ? this.state.formData.imgUrl : noteDetail.imgUrl
        };
      }

      return {
        ...noteDetail
      };
    });

    if (this.state.modal.state === "new" && this.state.formData.note) {
      updatedNotes.push({
        ...this.state.formData,
        id: uuid.v4(),
        trash: false
      });
    }
    this.setState({
      notes: updatedNotes,
      formData: {},
      modal: {
        show: false,
        note: {}
      }
    });
    this.saveOnLocaleStorage(updatedNotes);
  };

  onClickAddNotesCountHandler = () => {
    const updatedNotes = this.state.notes.map(noteDetail => {
      const { changedCount, count } = noteDetail;

      return {
        ...noteDetail,
        count: +count + +changedCount,
        changedCount: ""
      };
    });

    this.setState({
      notes: updatedNotes
    });
    this.saveOnLocaleStorage(updatedNotes);
  };

  onClickDeleteNoteHandler = ({ id }) => {
    const updatedNotes = this.state.notes.map(noteDetail => {
      if (noteDetail.id === id) {
        return {
          ...noteDetail,
          trash: true,
          count: 0
        };
      }
      return {
        ...noteDetail
      };
    });

    this.setState({
      notes: updatedNotes
    });
    this.saveOnLocaleStorage(updatedNotes);
  };

  onCloseModalHandler = () => {
    this.setState(prevState => {
      return {
        modal: {
          ...prevState.modal,
          show: !prevState.modal.show
        }
      };
    });
  };
  onSubmitFormHandler = formCollectedData => {
    this.setState({
      formData: formCollectedData
    });
    this.onCloseModalHandler();
  };

  onUndoTrashesNotesHandler = undoNote => {
    const updatedNotes = this.state.notes.map(noteDetail => {
      const { id } = noteDetail;
      if (id === undoNote.id) {
        return {
          ...noteDetail,
          trash: false,
          active: false,
          count:0
        };
      }

      return {
        ...noteDetail
      };
    });

    this.setState({ notes: updatedNotes });
    setToLocalStorage('notes', updatedNotes)
  };

  saveOnLocaleStorage = newState => {
    setToLocalStorage("notes", newState);
  };

  componentDidMount() {
    const lastNotes = getFromLocaleStorage("notes");
    const payHistory = getFromLocaleStorage("pay-history");
    if (lastNotes) {
      this.setState({
        notes: lastNotes,
        payHistory
      });
    }
  }
  render() {
    const { notes, payHistory } = this.state;

    return (
      <>
        <Notes
          notes={notes}
          payHistory={payHistory}
          onChangeNoteActivity={this.onChangeNoteActivity}
          onChangeNoteCount={this.onChangeNoteCount}
          onShowModalHandler={this.onShowModalHandler}
          onClickAddNotesCountHandler={this.onClickAddNotesCountHandler}
          onClickDeleteNoteHandler={this.onClickDeleteNoteHandler}
          onUndoTrashesNotesHandler={this.onUndoTrashesNotesHandler}
        />

        <Modal
          title="ویرایش اسکناس"
          visible={this.state.modal.show}
          onCancel={this.onCloseModalHandler}
          closable={false}
          footer={null}
          afterClose={this.onSubmitFormNoteHandler}
        >
          <NoteForm
            note={this.state.modal.note}
            showModal={this.state.modal.show}
            onSubmitFormHandler={this.onSubmitFormHandler}
          />
        </Modal>
      </>
    );
  }
}

export default Customer;
