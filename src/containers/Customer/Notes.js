import React, { Component } from "react";
import { Row, Button, Col, Card, Icon, Collapse, Empty } from "antd";
import Note from "./Note";
import PayHistory from "./PayHistory";

const { Panel } = Collapse;
class Notes extends Component {
  render() {
    /** deconstruct props */
    const { notes, payHistory, ...noteActions } = this.props;
    /** get active and inactive notes*/
    const activeNotes = notes.filter(({ active, trash }) => active && !trash);
    const inactiveNotes = notes.filter(({ active, trash }) => !active && !trash);
    const trashedNotes = notes.filter(({ trash }) => trash);

    /** form col props */
    const colProps = {
      lg: 6,
      md: 8,
      sm: 12
    };
    console.log(payHistory);
    
    return (
      <>
        <Row style={{ margin: "5px 0", padding: "0 5px" }} type={"flex"} justify={"center"}>
          <Col {...colProps}>
            <Card
              bodyStyle={{ flexGrow: 1 }}
              style={{
                height: "100%",
                display: "flex",
                alignItems: "center"
              }}
            >
              <Button block onClick={() => noteActions.onShowModalHandler({}, "new")}>
                <Icon type={"plus"} />
                اضافه کردن اسکناس
              </Button>
            </Card>
          </Col>
        </Row>
        <Collapse
          bordered={true}
          expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}
          accordion
        >
          <Panel header="اسکناس های فعال" key="1">
            <Note notes={activeNotes} {...noteActions} />
          </Panel>
          <Panel header="اسکناس های غیر فعال" key="2">
            <Note notes={inactiveNotes} {...noteActions} />
          </Panel>
          <Panel header="اسکناس های پاک شده" key="3">
            <Note notes={trashedNotes} {...noteActions} trashed />
          </Panel>
          <Panel header="تاریخچه پرداخت ها" key="4">
            {payHistory && payHistory.length > 0 && <PayHistory payHistory={payHistory} notes={notes} />}
            {!payHistory && <Empty />}
            {payHistory && payHistory.length ===0 && <Empty />}
          </Panel>
        </Collapse>
      </>
    );
  }
}

export default Notes;
