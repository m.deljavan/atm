import React, { Component } from "react";
import { Table } from "antd";

class PayHistory extends Component {
    
    render() {
        const { payHistory, notes } = this.props;
        
        /** collect data for show in table */
        const dataSet = payHistory.map((pay, index) => {
            const activeNotes = Object.keys(pay.payInfo).map(id => {
                return notes.find(note => note.id === id);
            }).filter(note => !!note);

            const paidNotes = Object.entries(pay.payInfo).reduce(
                (prev, [id, count]) => {
                    const noteDetail = notes.find(note => note.id === id);;
                    if (count > 0 && noteDetail) {
                        const { note, imgUrl } = noteDetail;
                        return prev.concat({ note, imgUrl, count });
                    }
                    return [...prev];
                },
                []
            );

            return {
                key: index,
                amount: pay.amount,
                rowCount: index + 1,
                paidNotes: paidNotes,
                activeNotes
            };
        });
        const columns = [
            {
                title: "#",
                dataIndex: "rowCount",
                key: "rowCount",
                align: "center"
            },
            {
                title: "جمع مبلغ پرداختی",
                dataIndex: "amount",
                key: "amount",
                align: "center"
            },
            {
                title: "اسکناس های پرداختی",
                dataIndex: "paidNotes",
                key: "paidNotes",
                align: "center",
                render: paidNotes => {
                    return paidNotes.map(({ note, imgUrl, count }, index) => {
                        return (
                            <div key={index} style={{ margin: 8 }}>
                                <img
                                    src={imgUrl}
                                    style={{
                                        width: 50,
                                        height: 30,
                                        margin: "0 5px"
                                    }}
                                />
                                <span style={{ marginRight: 5 }}>
                                    اسکناس {note} تومانی
                                </span>
                                <span style={{ marginRight: 5 }}>
                                    {" "}
                                    * {count}
                                </span>
                            </div>
                        );
                    });
                }
            },
            {
                title: "اسکناس های فعال",
                dataIndex: "activeNotes",
                key: "activeNotes",
                align: "center",
                render: activeNotes => {
                    return activeNotes.map(({ note, imgUrl }, index) => {
                        return (
                            <div key={index} style={{ margin: 8 }}>
                                <img
                                    src={imgUrl}
                                    style={{
                                        width: 50,
                                        height: 30,
                                        margin: "0 5px"
                                    }}
                                />
                                <span style={{ margin: 5 }}>
                                    اسکناس {note} تومانی
                                </span>
                            </div>
                        );
                    });
                }
            }
        ];
        return (
            <Table
                columns={columns}
                dataSource={dataSet}
                pagination={{ style: { direction: "ltr" }, position: "both" }}
            />
        );
    }
}

export default PayHistory;
