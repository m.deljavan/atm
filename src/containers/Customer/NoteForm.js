import React, { Component } from "react";
import { Form, Input, Upload, Button, Icon, Radio, InputNumber } from "antd";

class NoteForm extends Component {
    handleSubmit = e => {
        e.preventDefault();
        const { getFieldValue } = this.props.form;
        const collectField = ["note", "count", "imgUrl", "active"].reduce(
            (prev, curr) => {
                let formFieldData = getFieldValue(curr);
                console.log(formFieldData);
                
                if (curr === 'active') {
                    formFieldData = formFieldData === 'active'
                }
                if (curr === 'imgUrl' && formFieldData) {
                    
                    formFieldData = formFieldData[0].thumbUrl
                }
                return {
                    ...prev,
                    [curr]: formFieldData
                };
            },
            {}
        );
        this.props.onSubmitFormHandler(collectField);
    };
    componentDidUpdate() {
        if (!this.props.showModal) {
            this.props.form.resetFields();
        }
    }
    normFile = e => {
        if (Array.isArray(e)) {
            return e;
        }
        console.log(e);
        
        return e && e.fileList;
    };

    render() {

        const { getFieldDecorator } = this.props.form;
        const { count, note, active } = this.props.note;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 24 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 24 }
            }
        };

        return (
            
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                <Form.Item label="مبلغ اسکناس">
                    {getFieldDecorator("note", {
                        initialValue: note,
                        rules: [
                            {
                                required: true,
                                message: "لطفا مبلغ اسکناس را وارد نمایید"
                            }
                        ]
                    })(<Input />)}
                </Form.Item>

                <Form.Item label="تعداد">
                    {getFieldDecorator("count", {
                        initialValue: count
                    })(<InputNumber style={{flexGrow:1, width:'100%'}}/>)}
                </Form.Item>
                    <Form.Item label="وضعیت اسکناس">
                        {getFieldDecorator("active", {
                            initialValue: active ? 'active' : 'inactive'
                        })(<Radio.Group>
                            <Radio value="active">فعال</Radio>
                            <Radio value="inactive">غیرفعال</Radio>
                          </Radio.Group>)}
                    </Form.Item>
                <Form.Item>
                    {getFieldDecorator("imgUrl", {
                        valuePropName: "fileList",
                        getValueFromEvent: this.normFile
                    })(
                        <Upload name="note" listType="picture">
                            <Button
                                disabled={
                                    this.props.form.getFieldValue("imgUrl") &&
                                    this.props.form.getFieldValue("imgUrl")
                                        .length === 1
                                }
                            >
                                <Icon type="upload" /> بارگذاری عکس اسکناس
                            </Button>
                        </Upload>
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        ذخیره کن
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

NoteForm = Form.create({ name: "edit_note" })(NoteForm);

export default NoteForm;
