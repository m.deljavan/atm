import React, { Component } from "react";
import {
    Input,
    Button,
    Row,
    Col,
    Table,
    Divider,
    Tag,
    Alert,
    InputNumber
} from "antd";
import PayInfo from "./PayInfo";
import {
    getFromLocaleStorage,
    setToLocalStorage
} from "../../helper/localeStorage";
import {
    validationTotal,
    atm,
    validationPayNotes
} from "../../helper/clientHelper";
import Timer from "../Timer/Timer";

class Client extends Component {
    state = {
        amount: "",
        payInfo: {},
        error: "",
        notes: [],
        timeout: false,
        duration: 30,
        key: 1
    };

    onChangeAmountHandler = amount => {
        const newAmount = amount;
        this.setState({
            amount: newAmount,
            payInfo: {},
            error: ""
        });
    };

    onClickPayHandler = () => {
        const { amount } = this.state;
        const notes = getFromLocaleStorage("notes");
        const activeNotes = notes.filter(({ active }) => active);
        const validateTotal = validationTotal(amount, activeNotes);

        this.setState(state => ({
            duration: 30,
            key: state.key + 1,
            amount: ""
        }));

        if (activeNotes.length === 0) {
            this.setState({
                error: `دستگاه قادر به ارائه خدمات نمی باشد.`,
                payInfo: {}
            });

            return;
        }

        if (!validateTotal.valid) {
            this.setState({
                error: `پول موجود در دستگاه ${validateTotal.totalMoney} تومان می باشد. لطفا مبلغ کمتری انتخاب نمایید.`,
                payInfo: {}
            });

            return;
        }

        const payInfo = atm(amount, activeNotes);
        const validatePayNotes = validationPayNotes(
            payInfo,
            amount,
            activeNotes
        );

        if (!validatePayNotes.valid) {
            this.setState({
                error:
                    "با اسکناس های موجود در دستگاه امکان پرداخت مبلغ وارد شده نمی باشد.",
                payInfo: {}
            });

            return;
        }
        this.setState({
            payInfo: validatePayNotes.payInfo,
            error: "",
            notes
        });
        const newNotes = notes.map(note => {
            if (validatePayNotes.payInfo[note.id]) {
                return {
                    ...note,
                    count: note.count - validatePayNotes.payInfo[note.id]
                };
            }

            return {
                ...note
            };
        });
        setToLocalStorage("notes", newNotes);
        const payHistory = getFromLocaleStorage("pay-history") || [];
        setToLocalStorage(
            "pay-history",
            payHistory.concat({
                payInfo: { ...validatePayNotes.payInfo },
                amount: validatePayNotes.amount
            })
        );
    };

    timeoutHandler = () => {
        this.setState({ timeout: true, error: "" });
    };
    render() {
        return (
            <div>
                <Timer
                    key={this.state.key}
                    duration={this.state.duration}
                    timeoutHandler={this.timeoutHandler}
                />
                <Row type={"flex"} justify={"center"} style={{ marginTop: 30 }}>
                    <Col span={14}>
                        <InputNumber
                            style={{
                                textAlign: "center",
                                marginBottom: 10,
                                width: "100%"
                            }}
                            value={this.state.amount}
                            placeholder={"ملبغ مورد نظر را وارد نمایید"}
                            onChange={this.onChangeAmountHandler}
                            disabled={this.state.timeout}
                        />
                    </Col>
                    <Col span={10} style={{ padding: "0 5px", width: "auto" }}>
                        <Button
                            type={"primary"}
                            onClick={this.onClickPayHandler}
                            disabled={this.state.timeout || !this.state.amount}
                        >
                            تایید
                        </Button>
                    </Col>
                </Row>
                {this.state.timeout && (
                    <Alert
                        type={"info"}
                        message={
                            "زمان شما به پایان رسیده است. لطفا صفحه را  باز نشانی کنید."
                        }
                    />
                )}
                {this.state.error && (
                    <Alert type="error" message={this.state.error} />
                )}
                {!this.state.error &&
                    Object.keys(this.state.payInfo).length > 0 && (
                        <PayInfo
                            payData={this.state.payInfo}
                            notes={this.state.notes}
                        />
                    )}
            </div>
        );
    }
}

export default Client;
