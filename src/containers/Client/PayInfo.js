import React, { Component } from "react";
import { Table } from "antd";

class PayInfo extends Component {
    render() {
        const { payData, notes } = this.props;
        console.log(payData);
        
        let rowCounter = 1;
        const data = [];
        Object.entries(payData).forEach(([id, count]) => {
            if (count <= 0) {
                return;
            }
            const { note, imgUrl } = notes.find(note => note.id === id);
            data.push({
                key: note,
                note: {imgUrl, note},
                count,
                n: rowCounter++
            });
        });

        const columns = [
            {
                title: "#",
                dataIndex: "n",
                key: "n",
                width: 80,
                align: "center"
            },
            {
                title: "اسکناس",
                dataIndex: "note",
                key: "note",
                align: "center",
                render: ({imgUrl, note}) => (
                    <span>
                      <img src={imgUrl} style={{ width: 100, height: 50 }} />
                      <span style={{marginRight: 10}}>اسکناس {note} تومانی</span>
                      </span>
                )
            },
            {
                title: "تعداد",
                dataIndex: "count",
                key: "count",
                width: 150,
                align: "center"
            }
        ];

        return (
            <div>
                <Table
                    bordered={true}
                    style={{ backgroundColor: "white" }}
                    pagination={false}
                    columns={columns}
                    dataSource={data}
                />
            </div>
        );
    }
}

export default PayInfo;
